#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import init.common.rc
import init.carlife.rc
import init.nanopi3.usb.rc

on init
    export TSLIB_PLUGINDIR /system/lib/
    export TSLIB_CALIBFILE /system/etc/pointercal

# {{ for libfriendlyarm-things.so
    chmod 0666 /dev/watchdog
    chown system system /dev/watchdog

    chown system system /sys/class/gpio/export
    chown system system /sys/class/gpio/unexport
    chmod 0666 /sys/class/gpio/export
    chmod 0666 /sys/class/gpio/unexport

    chmod 0220 /sys/class/pwm/pwmchip0/export
    chown root system /sys/class/pwm/pwmchip0/export
    chmod 0220 /sys/class/pwm/pwmchip0/unexport
    chown root system /sys/class/pwm/pwmchip0/unexport

    chmod 0664 /sys/class/rtc/rtc0/date
    chown root system /sys/class/rtc/rtc0/date
    chmod 0664 /sys/class/rtc/rtc0/time
    chown root system /sys/class/rtc/rtc0/time
    chmod 0664 /sys/class/i2c-dev/i2c-3/device/3-002d/wakealarm
    chown root system /sys/class/i2c-dev/i2c-3/device/3-002d/wakealarm
# }}

on late-init
    write /sys/class/thermal/thermal_zone0/mode disabled

    write /sys/kernel/rcu_expedited 1

on early-fs
    write /dev/cpuset/foreground/cpus 0-7
    write /dev/cpuset/foreground/mems 0-7
    write /dev/cpuset/foreground/boost/cpus 0-7
    write /dev/cpuset/foreground/boost/mems 0-7
    write /dev/cpuset/background/cpus 0-7
    write /dev/cpuset/background/mems 0-7

on early-boot
    # set RLIMIT_MEMLOCK to 64MB
    setrlimit 8 67108864 67108864

on boot
    # nothing here yet

on post-fs
    chmod 0755 /sys/kernel/debug/tracing

on post-fs-data
    setprop vold.post_fs_data_done 1

on property:sys.boot_completed=1
    write /dev/cpuset/top-app/cpus 0-7
    write /dev/cpuset/foreground/boost/cpus 0-6
    write /dev/cpuset/foreground/cpus 0-6
    write /dev/cpuset/background/cpus 0
    write /dev/cpuset/system-background/cpus 0-6

    write /sys/class/thermal/thermal_zone0/mode enabled

    setprop sys.usb.config mtp,adb

# for telephony function
on property:ro.boot.noril=true
    setprop ro.radio.noril true
    stop ril-daemon

# hardware/ril/rild/rild.rc
service ril-daemon /system/bin/rild -l /system/lib64/libquectel-ril.so
    class main
    socket rild stream 660 root radio
    socket rild-debug stream 660 radio system
    user root
    group radio vpn cache inet misc audio log readproc wakelock
    capabilities BLOCK_SUSPEND NET_ADMIN NET_RAW
